#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QStringList>
#include <bitset>
#include <QLabel>
#include <bitset>
#include <QTableWidget>
#include <QPainter>

#include "des.h"

#include "key.h"



#define KEY_LENGTH 64

DES des;

bool isHex(const QChar c)
{
    if (
            (c.isDigit()) ||
            (('A' <= c.toUpper()) && (c.toUpper() <= 'F'))
       )
    {
        return true;
    }
    return false;
}

bool isHex(const QString s)
{
    for (int i = 0; i < s.length(); i++)
    {
        if (!isHex(s[i]))
        {
            return false;
        }
    }
    return true;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->permutationTable->hide();
    ui->permutationTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->dockWidget_2->hide();
    ui->dockWidget_2->setWindowTitle("F function");
    ui->permutationTable_4->hide();
    ui->permutationTable_2->hide();
    ui->expansionTable_1->hide();
    ui->permutationTable_3->hide();
}

MainWindow::~MainWindow()
{
    delete ui;
}


unsigned long long getValFromHex(QString hex)
{
    hex.remove(' ');
    return hex.toULongLong(nullptr, 16);
}

unsigned long long getValFromBinary(QString binary)
{
    binary.remove('-');
    return binary.toULongLong(nullptr, 2);
}

QString getHexFromVal(long long value, int size=0)
{
    QString hex = QString::number(value, 16);
    if (size == 0)
    {
        return hex;
    }
    for (int i = hex.length(); i < size; i++)
    {
        hex.insert(0, '0');
    }
    return hex;
}

QString getBinaryFromVal(long long value, int size=0)
{
    QString binary = QString::number(value, 2);
    if (size == 0)
    {
        return binary;
    }
    for (int i = binary.length(); i < size; i++)
    {
        binary.insert(0, '0');
    }
    return binary;
}

QString prettyString(QString str, int base)
{
    switch(base)
    {
        case 2:
        {
            QString binary = "";
            int i;
            for (i = 0; i < str.length()-8; i+=8)
            {
                binary += str.mid(i, 8) + '-';
            }
            binary += str.mid(i, 8);
            return binary;
        }

        case 10:
        {
            QString dec = "";
            int i;
            for (i = 0; i < str.length()-3; i+=3)
            {
                dec += str.mid(i, 3) + '.';
            }
            dec += str.mid(i, 3);
            return dec;
        }

        case 16:
        {
            QString hex = "";
            int i;
            for (i = 0; i < str.length()-2; i+=2)
            {
                hex += str.mid(i, 2) + ' ';
            }
            hex += str.mid(i, 2);
            return hex;
        }
    }

    return str;
}

QString getCleanString(QString str)
{
    str.remove(' ');
    str.remove('-');
    return str;
}

unsigned long long computeParity(QString binary)
{
    unsigned long long result = 0;
    QString resultString = "";
    QStringList list = binary.split('-');
    for (auto currentByte : list)
    {
        if (0 == currentByte.left(currentByte.length() - 1).count('1') % 2)
        {
            currentByte[currentByte.length()-1] = '0';
        }
        else
        {
            currentByte[currentByte.length()-1] = '1';
        }
        resultString += currentByte;
    }
    return getValFromBinary(resultString);
}

unsigned long long removeParity(QString binary)
{
    unsigned long long result = 0;
    QString resultString = "";
    QStringList list = binary.split('-');
    for (auto currentByte : list)
    {
        currentByte.remove(currentByte.length()-1, 1);
        resultString += currentByte;
    }
    return getValFromBinary(resultString);
}

void MainWindow::setRoundValues(int round, std::tuple<std::string, std::string> splittedKey)
{
    des.computeRoundKeys(std::get<0>(splittedKey), std::get<1>(splittedKey));
    std::vector<std::tuple<std::string, std::string>> splittedRKeyb = des.shiftsb;
    std::vector<std::tuple<std::string, std::string>> splittedRKey = des.shifts;

    std::vector<std::string> rkb = des.rkb;
    std::vector<std::string> rk = des.rk;

    foreach(QGroupBox *group, ui->scrollAreaWidgetContents->findChildren<QGroupBox*>())
    {
        foreach(QLabel *label, group->findChildren<QLabel*>())
        {
            QString hexKeyRound;
            QString binKeyRound;
            QString hexLeftKeyShift;
            QString binaryLeftKeyShift;
            QString hexRightKeyShift;
            QString binaryRightKeyShift;
            hexKeyRound.sprintf("hexKeyRound_%d", round+1);
            binKeyRound.sprintf("binKeyRound_%d", round+1);
            hexLeftKeyShift.sprintf("hexLeftKeyShift_%02d", round+1);
            binaryLeftKeyShift.sprintf("binaryLeftKeyShift_%02d", round+1);
            hexRightKeyShift.sprintf("hexRightKeyShift_%02d", round+1);
            binaryRightKeyShift.sprintf("binaryRightKeyShift_%02d", round+1);

            if (-1 != label->objectName().indexOf(hexKeyRound))
            {
                label->setText(QString::fromStdString(rk[round]));
            }
            if (-1 != label->objectName().indexOf(binKeyRound))
            {
                label->setText(QString::fromStdString(rkb[round]));
            }
            if (-1 != label->objectName().indexOf(hexLeftKeyShift))
            {
                label->setText(QString::fromStdString(std::get<0>(splittedRKey[round])));
            }
            if (-1 != label->objectName().indexOf(binaryLeftKeyShift))
            {
                label->setText(QString::fromStdString(std::get<0>(splittedRKeyb[round])));
            }
            if (-1 != label->objectName().indexOf(hexRightKeyShift))
            {
                label->setText(QString::fromStdString(std::get<1>(splittedRKey[round])));
            }
            if (-1 != label->objectName().indexOf(binaryRightKeyShift))
            {
                label->setText(QString::fromStdString(std::get<1>(splittedRKeyb[round])));
            }
        }
    }
}

void MainWindow::setTextEncr(int round)
{

    std::vector<std::tuple<std::string, std::string>> splittedRTextb = des.splitTextb;
    std::vector<std::tuple<std::string, std::string>> splittedRText = des.splitText;
    std::cout << "size: " << des.splitText.size();
    foreach(QGroupBox *group, ui->scrollAreaWidgetContents->findChildren<QGroupBox*>())
    {
        foreach(QLabel *label, group->findChildren<QLabel*>())
        {
            QString hexLeftText;
            QString binaryLeftText;
            QString hexRightText;
            QString binaryRightText;

            hexLeftText.sprintf("hexLeftText_%d", round);
            binaryLeftText.sprintf("binaryLeftText_%d", round);
            hexRightText.sprintf("hexRightText_%d", round);
            binaryRightText.sprintf("binaryRightText_%d", round);

            if (-1 != label->objectName().indexOf(hexLeftText))
            {
                label->setText(QString::fromStdString(std::get<0>(splittedRText[round])));
            }
            if (-1 != label->objectName().indexOf(binaryLeftText))
            {
                label->setText(QString::fromStdString(std::get<0>(splittedRTextb[round])));
            }
            if (-1 != label->objectName().indexOf(hexRightText))
            {
                label->setText(QString::fromStdString(std::get<1>(splittedRText[round])));
            }
            if (-1 != label->objectName().indexOf(binaryRightText))
            {
                label->setText(QString::fromStdString(std::get<1>(splittedRTextb[round])));
            }
        }
    }
}

void MainWindow::on_keyHexBox_textEdited(const QString &arg1)
{
    QString keyString = arg1;

    unsigned long long value = getValFromHex(keyString);

    int currentCursorPos = ui->keyHexBox->cursorPosition();
    ui->keyHexBox->setText(prettyString(getHexFromVal(value), 16).toUpper());
//    ui->keyHexBox->setCursorPosition(currentCursorPos + 1);
    ui->keyBinaryBox->setText(prettyString(getBinaryFromVal(value, 64), 2));
}

QString parityHighlight(QString binary)
{
    QString richTextBgTagFirst = "<span style=\"color:red;\">";
    QString richTextBgTagSecond = "</span>";
    int pos = 0;
    QString result = "";
    while (-1 != (pos = binary.indexOf('-')))
    {
        result += binary.mid(0, pos - 1) + richTextBgTagFirst + binary[pos-1] + richTextBgTagSecond + binary[pos];
        binary.remove(0, pos + 1);
    }
    result += binary.mid(0, binary.length() - 1) + richTextBgTagFirst + binary[binary.length()-1] + richTextBgTagSecond;

//    QString result = binary.mid(0, pos-1) + richTextBgTagFirst + binary[pos-1] + richTextBgTagSecond + binary.mid(pos, binary.length() - pos);
    return result;
}

void populatePermutationTable(QTableWidget *table, int *permutation, int permutationSize, std::string key)
{
    if (0 == permutationSize)
    {
        // TODO: Interface;
        qDebug() << "permutationSize is 0";
        return;
    }
    table->setColumnCount(permutationSize);
    table->setRowCount(2);
    QStringList verticalLabels = {"PC-1", "PC-1(key)"};
    table->setVerticalHeaderLabels(verticalLabels);

    for (int column = 0; column < permutationSize; column++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem();
        newItem->setText(QString::number(permutation[column]));
        table->setItem(0, column, newItem);
    }

    for (int column = 0; column < permutationSize; column++)
    {
        QTableWidgetItem *newItem = new QTableWidgetItem();
        newItem->setText(QChar(key[column]));
        table->setItem(1, column, newItem);
    }

}

void populateSTable(QTableWidget* table, int s[4][16])
{
    table->setColumnCount(16);
    table->setRowCount(4);

    for (int row = 0; row < 4; row++)
    {
        for (int column = 0; column < 16; column++)
        {
            QTableWidgetItem *newItem = new QTableWidgetItem();
            newItem->setText(QString::number(s[row][column]));
            table->setItem(row, column, newItem);
        }
    }
}

void MainWindow::on_keyHexBox_selectionChanged()
{
//    qDebug() << "Selection";
}

void MainWindow::on_keyHexBox_cursorPositionChanged(int arg1, int arg2)
{
//    qDebug() << "Cursor: " << arg1 << " " << arg2;
}

void MainWindow::startEncrypt()
{
    std::string key;

    unsigned long long keyValue = computeParity(ui->keyBinaryBox->text());

    QString originalHexKey = getHexFromVal(keyValue).toUpper();
    QString originalBinaryKey = getBinaryFromVal(keyValue, 64);

    // Show first group keys in interface
    ui->keyHexBox->setText(prettyString(originalHexKey, 16));
    ui->keyBinaryBox->setText(prettyString(originalBinaryKey, 2));
    ui->keyBinaryParityLabel->setText(parityHighlight(prettyString(originalBinaryKey, 2)));

    // Remove parity bits
    keyValue = removeParity(ui->keyBinaryBox->text());
    QString noParityHexKey = getHexFromVal(keyValue).toUpper();
    QString noParityBinaryKey = getBinaryFromVal(keyValue, 56);

    // Show key without pairty bits
    ui->keyHexNParityLabel->setText(prettyString(noParityHexKey, 16));
    ui->keyBinaryNParityLabel->setText(prettyString(noParityBinaryKey, 2));

    // Key permutation
    key = originalBinaryKey.toStdString();
    des.setKey(key);
    key = des.permute(key, des.keyp, 56);
    QString pHexKey = QString::fromStdString(bin2hex(key)).toUpper();
    QString pBinaryKey = QString::fromStdString(key);
    populatePermutationTable(ui->permutationTable, des.keyp, 56, key);
    ui->hexPerKeyLabel->setText(prettyString(pHexKey, 16));

    // Split key
    std::tuple<std::string, std::string> splittedKey = des.split(key);
    QString leftBinKey = QString::fromStdString(std::get<0>(splittedKey));
    QString rightBinKey = QString::fromStdString(std::get<1>(splittedKey));
    QString leftHexKey = QString::fromStdString(bin2hex(std::get<0>(splittedKey))).toUpper();
    QString rightHexKey = QString::fromStdString(bin2hex(std::get<1>(splittedKey))).toUpper();

    ui->hexLeftKey0->setText(leftHexKey);
    ui->hexRightKey0->setText(rightHexKey);
    ui->binaryLeftKey0->setText(leftBinKey);
    ui->binaryRightKey0->setText(rightBinKey);

    // Round keys
    for (int round = 0; round < 16; round++)
    {
        setRoundValues(round, splittedKey);
    }


    std::string text;

    unsigned long long textValue = getValFromBinary(ui->ptBinaryBox->text());

//    QString originalHexKey = getHexFromVal(textValue).toUpper();
//    QString originalBinaryKey = getBinaryFromVal(textValue, 64);
    QString originalPlainTextHex = ui->ptHexBox->text().remove(" ");
    QString originalPlainTextBin = ui->ptBinaryBox->text().remove("-");

    // Show first group text in interface
    ui->ptHexBox->setText(prettyString(originalPlainTextHex, 16));
    ui->ptBinaryBox->setText(prettyString(originalPlainTextBin, 2));

    // Text permutation
    text = bin2hex(originalPlainTextBin.toStdString());
    des.setPlainText(text);
    qDebug() << QString::fromStdString(des.key);
    qDebug() << QString::fromStdString(des.plainText);
    if (des.key == "")
    {
        qDebug() << "Please choose a key";
        std::cout << "Please choose a key";
    }
    des.encrypt();

    qDebug() << QString::fromStdString(des.perText);
    QString pHexText = QString::fromStdString(bin2hex(des.perText)).toUpper();
    QString pBinaryText = QString::fromStdString(des.perText);
    populatePermutationTable(ui->permutationTable_2, des.initialPermutation, 64, des.perText);
    ui->hexPerTextLabel->setText(prettyString(pHexText, 16));

    // Split text
    for (int round = 0; round < 17; round++)
    {
        setTextEncr(round);
    }
//    ui->hexLeftText_0->setText(QString::fromStdString(std::get<0>(des.splitText[0])));
//    ui->binaryLeftText_0->setText(QString::fromStdString(std::get<0>(des.splitTextb[0])));
//    ui->hexRightText_0->setText(QString::fromStdString(std::get<1>(des.splitText[0])));
//    ui->binaryRightText_0->setText(QString::fromStdString(std::get<1>(des.splitTextb[0])));

    ui->hexPerTextLabel_2->setText(QString::fromStdString(des.cipherText));
    ui->binCiphertext->setText(QString::fromStdString(hex2bin(des.cipherText)));
    populatePermutationTable(ui->permutationTable_4, des.finalPerm, 64, hex2bin(des.cipherText));

}

void MainWindow::on_keyHexBox_returnPressed()
{


//    ui->scrollArea->ensureWidgetVisible(ui->shiftGroupR_07, 50, 300);

}

void MainWindow::setFunctionWindow(int round)
{
    // F
    // Expantion
    ui->hexExp_1->setText(QString::fromStdString(des.expand[round]));
    populatePermutationTable(ui->expansionTable_1, des.expD, 48, des.expandb[round]);

    ui->hexKeyRound_in->setText(QString::fromStdString(des.rk[round]));
    ui->binKeyRound_in->setText(QString::fromStdString(des.rkb[round]));

    // 6bits
    QString hexExp6;
    QString binExp6;
    hexExp6.sprintf("hexExp6_");
    binExp6.sprintf("binExp6_");

    foreach(QLabel* label, ui->shiftGroupR_3->findChildren<QLabel*>())
    {
        QString name = label->objectName();
        if (-1 != label->objectName().indexOf(hexExp6))
        {
            int index = name[name.length()-1].digitValue()-1;
            label->setText(QString::fromStdString(bin2hex(des.fXorb[round].substr(index*6, 6))));
        }
        if (-1 != label->objectName().indexOf(binExp6))
        {
            int index = name[name.length()-1].digitValue()-1;
            label->setText(QString::fromStdString(des.fXorb[round].substr(index*6, 6)));
        }
    }

    // SBoxes
    QString sBox;
    sBox.sprintf("s_");
    foreach(QTableWidget* table, ui->scrollAreaWidgetContents_2->findChildren<QTableWidget*>())
    {
        QString name = table->objectName();
        if (-1 != name.indexOf(sBox))
        {
            int index = name[name.length()-1].digitValue()-1;
            populateSTable(table, des.sBox[index]);

            int row = 2 * int(des.fXorb[round][index * 6] - '0') + int(des.fXorb[round][index * 6 + 5] - '0');
            int col = 8 * int(des.fXorb[round][index * 6 + 1] - '0') + 4 * int(des.fXorb[round][index * 6 + 2] - '0') + 2 * int(des.fXorb[round][index * 6 + 3] - '0') + int(des.fXorb[round][index * 6 + 4] - '0');
            table->setCurrentCell(row, col);
        }
    }

    // Op
    QString hexOp;
    QString binOp;
    hexOp.sprintf("hexOp_");
    binOp.sprintf("binOp_");

    foreach(QLabel* label, ui->shiftGroupR_5->findChildren<QLabel*>())
    {
        QString name = label->objectName();
        if (-1 != label->objectName().indexOf(hexOp))
        {
            int index = name[name.length()-1].digitValue()-1;
            label->setText(QString::fromStdString(bin2hex(des.opSb[round].substr(index*4, 4))));
        }
        if (-1 != label->objectName().indexOf(binOp))
        {
            int index = name[name.length()-1].digitValue()-1;
            label->setText(QString::fromStdString(des.opSb[round].substr(index*4, 4)));
        }
    }

    // Second permutatin
    ui->hexPer_out->setText(QString::fromStdString(des.per2[round]));
    populatePermutationTable(ui->permutationTable_3, des.sPer, 32, des.per2b[round]);
}

void MainWindow::on_keyBinaryBox_textEdited(const QString &arg1)
{
    QString binaryKeyString = arg1;
    int currentCursorPos = ui->keyBinaryBox->cursorPosition();

    if (binaryKeyString.length() > 64)
    {
        binaryKeyString.remove(0, 1);
    }

    unsigned long long value = getValFromBinary(binaryKeyString);
    ui->keyHexBox->setText(prettyString(getHexFromVal(value), 16).toUpper());
    ui->keyBinaryBox->setText(prettyString(getBinaryFromVal(value, 64), 2));
    ui->keyBinaryBox->setCursorPosition(currentCursorPos - 1);
    ui->keyBinaryParityLabel->setText(parityHighlight(prettyString(getBinaryFromVal(value, 64), 2)));
}

void MainWindow::on_pushButton_clicked()
{
    if (ui->permutationTable->isVisible())
    {
        ui->permutationTable->hide();
    }
    else
    {
        ui->permutationTable->show();
    }
}

void MainWindow::on_ptHexBox_textEdited(const QString &arg1)
{
    QString textString = arg1;

    unsigned long long value = getValFromHex(textString);

    int currentCursorPos = ui->ptHexBox->cursorPosition();
    ui->ptHexBox->setText(prettyString(getHexFromVal(value), 16).toUpper());
//    ui->ptHexBox->setCursorPosition(currentCursorPos);
    ui->ptBinaryBox->setText(prettyString(getBinaryFromVal(value, 64), 2));
}

void MainWindow::on_ptBinaryBox_textEdited(const QString &arg1)
{
    QString binaryTextString = arg1;
    int currentCursorPos = ui->ptBinaryBox->cursorPosition();

    if (binaryTextString.length() > 64)
    {
        binaryTextString.remove(0, 1);
    }

    unsigned long long value = getValFromBinary(binaryTextString);
    ui->ptHexBox->setText(prettyString(getHexFromVal(value), 16).toUpper());
    ui->ptBinaryBox->setText(prettyString(getBinaryFromVal(value, 64), 2));
    ui->ptBinaryBox->setCursorPosition(currentCursorPos - 1);
}

void MainWindow::on_ptHexBox_returnPressed()
{

}

void MainWindow::on_function_1_clicked()
{
    setFunctionWindow(0);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_4_clicked()
{
    setFunctionWindow(1);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_5_clicked()
{
    setFunctionWindow(2);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_6_clicked()
{
    setFunctionWindow(3);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_7_clicked()
{
    setFunctionWindow(4);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_8_clicked()
{
    setFunctionWindow(5);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_9_clicked()
{
    setFunctionWindow(6);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_10_clicked()
{
    setFunctionWindow(7);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_12_clicked()
{
    setFunctionWindow(8);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_13_clicked()
{
    setFunctionWindow(9);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_14_clicked()
{
    setFunctionWindow(10);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_15_clicked()
{
    setFunctionWindow(11);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_16_clicked()
{
    setFunctionWindow(12);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_17_clicked()
{
    setFunctionWindow(13);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_18_clicked()
{
    setFunctionWindow(14);

    ui->dockWidget_2->show();
}

void MainWindow::on_function_19_clicked()
{
    setFunctionWindow(15);

    ui->dockWidget_2->show();
}

void MainWindow::on_pushButton_5_clicked()
{
    if (ui->permutationTable_4->isVisible())
    {
        ui->permutationTable_4->hide();
    }
    else
    {
        ui->permutationTable_4->show();
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    if (ui->permutationTable_2->isVisible())
    {
        ui->permutationTable_2->hide();
    }
    else
    {
        ui->permutationTable_2->show();
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    if (ui->expansionTable_1->isVisible())
    {
        ui->expansionTable_1->hide();
    }
    else
    {
        ui->expansionTable_1->show();
    }
}

void MainWindow::on_pushButton_4_clicked()
{
    if (ui->permutationTable_3->isVisible())
    {
        ui->permutationTable_3->hide();
    }
    else
    {
        ui->permutationTable_3->show();
    }
}

void MainWindow::on_next_1_clicked()
{
    startEncrypt();
//    ui->scrollArea->ensureWidgetVisible(ui->g3, 50, ui->centralwidget->size().height()/2);
}

void MainWindow::on_comboBox_activated(const QString &arg1)
{
    if (arg1 == "Encrypt")
    {
        des.encryptTrue = true;
        ui->g1_2->setTitle("                                                 Plaintext                                                    ");
        ui->g3_3->setTitle("                                             Ciphertext                                             ");
    }
    if (arg1 == "Decrypt")
    {
        des.encryptTrue = false;
        ui->g1_2->setTitle("                                                 Ciphertext                                                    ");
        ui->g3_3->setTitle("                                             Plaintext                                             ");
    }
}

void MainWindow::on_pushButton_6_clicked()
{
    QImage img(ui->scrollAreaWidgetContents->size(), QImage::Format_RGB888);
    QPainter paintImg(&img);
    ui->scrollAreaWidgetContents->render(&paintImg);
    img.save("./" + ui->comboBox->currentText() + "_" + ui->keyHexBox->text().remove(' ') + "_" + ui->ptHexBox->text().remove(' ') + ".jpg");
}
