#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_keyHexBox_textEdited(const QString &arg1);

    void on_keyHexBox_selectionChanged();

    void on_keyHexBox_cursorPositionChanged(int arg1, int arg2);

    void on_keyBinaryBox_textEdited(const QString &arg1);

    void on_keyHexBox_returnPressed();

    void on_pushButton_clicked();

    void setRoundValues(int round, std::tuple<std::string, std::string> splittedKey);

    void setTextEncr(int round);

    void on_ptHexBox_textEdited(const QString &arg1);

    void on_ptBinaryBox_textEdited(const QString &arg1);

    void on_ptHexBox_returnPressed();

    void on_function_1_clicked();

    void startEncrypt();

    void setFunctionWindow(int round);

    void on_function_4_clicked();

    void on_function_5_clicked();

    void on_function_6_clicked();

    void on_function_7_clicked();

    void on_function_8_clicked();

    void on_function_9_clicked();

    void on_function_10_clicked();

    void on_function_12_clicked();

    void on_function_13_clicked();

    void on_function_14_clicked();

    void on_function_15_clicked();

    void on_function_17_clicked();

    void on_function_16_clicked();

    void on_function_18_clicked();

    void on_function_19_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_next_1_clicked();

    void on_comboBox_activated(const QString &arg1);

    void on_pushButton_6_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
